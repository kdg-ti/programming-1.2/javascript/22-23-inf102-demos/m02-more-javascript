const exam= {
  term: "P3",
  lowStake:{
    maxScore:10,
    week: 4
  },
  highStake:{
    maxScore:90,
    week: 7
  }
};


console.log (exam);
console.log(Object.keys(exam));
console.log(exam.term);
console.log(exam["term"]);
console.log("\nLooping over keys:")
function printKeys(thing) {
  for (let key in thing) {
    //console.log("attribute %s with value %s",key,exam[key]);
    //console.log("Plain stringify")
    console.log(`attribute ${key} of type ${typeof thing[key]} with value ${JSON.stringify(thing[key])}`);
    // console.log("Stringify  with space")
    // console.log(`attribute ${key} with value ${JSON.stringify(exam[key],null," ")}`);
  }
}
printKeys(exam);
printKeys({firstName:"jan",job:"teacher"})

function printKeysRecursive(thing) {
  console.log(`Calling printKeyRecursive on  ${JSON.stringify(thing)}`)
  for (let key in thing) {
    if(typeof thing[key] === "object"){
      printKeysRecursive(thing[key])
    } else {
      console.log(`attribute ${key} of type ${typeof thing[key]} with value ${thing[key]}`);
    }
  }
}
console.log("\nOnly printing primitive attributes:")
printKeysRecursive(exam);

console.log("\nPrinting keys in an array:")

const days=[31,28,31,30,31,30,31,31,30,31,30,31]
printKeys(days);