function max(...numbers) {
  let result = -Infinity;
  for (let number of numbers) {
    if (number > result) result = number;
  }
  return result;
}
console.log(max(1,2,3));
///====

  let actions = [ "pizza", "running"];
let events= ["work", "touched tree", ...actions];
let events2= ["work", "touched tree", actions];
console.log(events);
console.log(events2);