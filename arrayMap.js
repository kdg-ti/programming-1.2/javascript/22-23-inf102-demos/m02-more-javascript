let john = {name: "John", age: 25};
let pete = {name: "Pete", age: 30};
let mary = {name: "Mary", age: 28};

let users = [john, pete, mary];

result = [];
for (let user of users) {
  result.push(user.name);
}
console.log(result);
console.log(users.map(user => user.name))