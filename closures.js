
let things = "crows"

function showMessage(number) {console.log(number + " " + things);}

function makeCounter() {
  let count = 2;

  return function () {
    return count++;
  };
}



let counter = makeCounter();
let other = makeCounter();
console.log(counter());
 console.log(counter());
 console.log(counter());
 console.log(other());
 console.log(other());
//console.log(count);

// showMessage(counter()); // 2 crows
// showMessage(counter()); // 3 crows
// showMessage(counter()); // 4 crows
//


function multiplier(factor){
  return function (number){
    return number * factor;
  }
}
const double = multiplier(2);
const triple = multiplier(3);

console.log(double(5));
console.log(double(3));
console.log(double);

console.log(triple(5));


