const days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

// function daysInMonthsRecursive(months) {
//   if(months.length == 0){
//     return 0
//   } else{
//     return months[months.length-1] + daysInMonthsRecursive()
//   }
//   let result = 0;
//   for (let i = 0; i < months.length; i++) {
//     result += months[i];
//   }
//   return result;
// }
function daysInMonths(months) {
  let result = 0;
  for (let i = 0; i < months.length; i++) {
    result += months[i];
  }
  return result;
}

function daysInMonthsEnhancedFor(months) {
  let result = 0;
  for (let i of months) {
    result += i;
  }
  return result;
}

function daysInMonthsForEach(months) {
  let result = 0;
  months.forEach(i => result += i);
  return result;
}

function daysInMonthsWithEvenDays(months) {
  let result = 0;
  months
    .filter(i => i % 2 === 0)
    .forEach(i => result += i);
  return result;
}

function daysInMonthsReduce(months) {
  //let result = 0;
  return months.reduce((acc,i) => acc+i);
  //return result;
}

function isEven(i){
  return i%2===0;
}

function daysInMonthsWithEvenDaysFuntion(months) {
  let result = 0;
  months
    .filter(isEven)
    .forEach(i => result += i);
  return result;
}



console.log(daysInMonths(days));
console.log(daysInMonthsEnhancedFor(days));
console.log(daysInMonthsWithEvenDays(days));


let students = ["William", "Sergio", "Antonio", "Smith", "Jan", "Chike"];
console.log(students
  .sort((a, b) => a > b ? 1 : -1)
  .map(element => element.toUpperCase())
  .filter(a => a.includes("A"))
);
//console.log(students)
