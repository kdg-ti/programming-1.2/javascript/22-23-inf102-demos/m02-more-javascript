function power(number,exponent){
  let result = 1;
  for (let i = 0;i<exponent;i++){
    result *= number;
  }
  return result;
}
console.log(power(2,32));

function powerRecursive(number,exponent){
  if(exponent<0){
    return 1/powerRecursive(number,-exponent)
  }
  console.log(`Power of ${number} to ${exponent}`)
  if(exponent === 0){
    return 1;
  } else{
    return powerRecursive(number,exponent-1) * number;
  }
}

// console.log(powerRecursive(2,32));
// console.log(powerRecursive(2,0));
// console.log(powerRecursive(2,-2));
// console.log(powerRecursive(-2,31));
console.log(powerRecursive(2,1));

