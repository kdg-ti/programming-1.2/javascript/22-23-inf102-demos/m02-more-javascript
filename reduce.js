const days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

function daysInMonthsReduce(months) {
  //let result = 0;
  return months.reduce((acc,i) => acc+i);
  //return result;
}


console.log("Using reduce",daysInMonthsReduce(days));

const example = [1,2,3,4];
function sumProduct (numbers){
  // this does not produced the right result, because the accumulator is initialised
  // with the first element in the array
 // return numbers.reduce((acc, i, idx) => acc + i * idx);
  return numbers.reduce((smith, i, idx) => smith + i * idx,
    0);
}

console.log(sumProduct(example))